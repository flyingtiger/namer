<?php
namespace Ryantxr\Storygen\Namer;

class Personality {
    public $traits = [];
    public $flaws = [];

    function __construct() {
        $this->generateFlaws();
        $this->generateTraits();
    }

    function __toString() {
        $traits = '';
        foreach($this->traits as $key => $value) {
            if ( ! empty($traits) ) $traits .= ', ';
            $traits .= "$key = $value";
        }
        
        //implode(', ', $this->traits);
        $flaws = implode(', ', $this->flaws);
        return sprintf("Traits: %s  \nFlaws: %s", $traits, $flaws);
    }

    function generateTraits() {
        foreach(self::Traits as $key => $value) {
            $this->traits[$key] = $value[mt_rand(0,count($value)-1)];
        }
    }
    
    function generateFlaws() {
        $count = count(self::Flaws);

        for($i=0; $i<3; $i++) {
            $x = mt_rand(0,$count-1);
            $flawIndexes = [];
            if ( ! in_array($x, $this->flaws) ) {
                $flawIndexes[] = $x;
                $this->flaws[] = self::Flaws[$x];
            }
        }
    }

    Const Traits = [
        'Courteous'       => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Risk-Taking'     => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Ambitious'       => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Curious'         => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Self-Controlled' => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Nurturing'       => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Trusting'        => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Honest'          => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Loyal'           => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Affectionate'    => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Romantic'        => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Flirty'          => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Sympathetic'     => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Altruistic'      => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Optimistic'      => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Observant'       => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Logical'         => ['Never', 'Rarely', 'Occasionally', 'Sometimes', 'Generally', 'Usually', 'Typically', 'Often'],
        'Social'          => [
            'Very solitary', 'Usually solitary', 'Fairly solitary', 'Somewhat solitary',
            'Very shy', 'Usually shy', 'Fairly shy', 'Somewhat shy', 
            'Somewhat outgoing', 'Fairly outgoing', 'Usually outgoing', 'Very outgoing',
        ],
        'Emotions'        => [
            'Somewhat stable', 'Fairly stable', 'Usually stable', 'Very stable',
            'Somewhat controlled', 'Fairly controlled', 'Usually controlled', 'Very controlled',
            'Somewhat unstable', 'Fairly unstable', 'Usually unstable', 'Very unstable'
        ],
    ];
    

    const Flaws = [
        'verbally abusive',
        'judgmental',
        'allergic to a common foodstuff',
        'neophobic',
        'bigoted',
        'sadistic',
        'selfish ',
        'whiny',
        'controlling',
        'afraid of a common situation',
        'impatient',
        'controlling',
        'emotionally fragile',
        'petty',
        'arrogant',
        'judgmental',
        'emotionally fragile',
        'chronically ill',
        'power-hungry',
        'afraid of a common animal',
        'greedy',
        'short-tempered',
        'dangerously obsessive',
        'vindictive',
        'emotionally fragile',
        'addicted to a dangerous substance',
        'gullible',
        'lazy',
        'liar',
        'whiny',
        'afraid of a common situation',
        'paranoid',
        'self-important',
        'sadistic',
        'physically abusive',
        'power-hungry',
        'lazy',
        'short-sighted',
        'fearful',
        'arrogant',
        'manipulative',
        'dangerously obsessive',
        'overly dramatic',
        'self-important',
        'paranoid',
        'sadistic',
        'petty',
        'self-important',
        'power-hungry',
        'xenophobic',
        'socially awkward',
        'selfish',
    ];
}