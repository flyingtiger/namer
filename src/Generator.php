<?php
namespace Ryantxr\Storygen\Namer;

class Generator
{

    static function generateName($nameType)
    {

        $g = strtolower($nameType);
        $name = null;
        switch($g) {
            case 'm':
            case 'f':
            case 'surname':
                $rand = mt_rand(0, count(self::$names[$g])-1);
                $name = self::$names[$g][$rand];
                break;
            default:

                break;
        }

        return $name;
    }

    static $names = [
        'f' => [
            'Abigail', 'Alice', 'Amber','Amelia', 'Annabelle','Aria','Ava',
            'Amelie','Anna','Aisha','Arabella','Adams','Alicia',
            'Bella','Beatrice','Belinda',
            'Charlotte', 'Chloe', 'Clara', 'Casi', 'Casey','Camilla',
            'Daisy','Darcie','Darcey','Darcy','Ditch',
            'Ella', 'Elsie', 'Emily', 'Eva', 'Evie','Elisa','Erin',
            'Evelyn','Esme','Emilia','Eliza','Erin','Eleanor','Ellie',
            'Elizabeth','Emma',
            'Freya', 'Florence', 'Francesca','Felicity',
            'Georgia','Gracie','Grace',
            'Harper','Harriet','Holly','Hannah','Heidi',
            'Isla', 'Isabella', 'Isabelle','Ivy','Imogen','Iris',
            'Jasmine','Jessica','Julia',
            'Kaci', 'Kasey','Kori','Kim','Kallie', 'Kathleen','Karen','Kate','Keira',
            'Lily','Layla','Lola','Lucy','Lilly','Lottie','Luna','Leah','Lydia',
            'Lexi','Lauren',
            'Maya', 'Mia', 'Matilda', 'Millie', 'Maisie', 'Molly', 'Maria', 'Martha', 'Maryam',
            'Maddison', 'Megan', 'Mila','Marya','Michelle',
            'Nancy', 'Nicola', 'Nikki', 'Nabila', 'Nadia', 'Nadina', 'Naeva', 'Naima', 'Naira', 'Nairi','Nicole',
            'Najila', 'Noreen', 'Nyla', 'Narda', 'Naomi', 'Narelle', 'Nasya', 'Natalie', 'Natania', 'Nea',
            'Poppy', 'Phoebe', 'Penelope', 'Paige', 'Paisley', 'Pakpao', 'Palma', 'Pam','Paris',
            'Pamela', 'Panthea', 'Paris', 'Parke', 'Pascale', 'Paula', 'Paulina', 'Penny', 'Perla', 'Peyton',
            'Phillippa', 'Phoebe', 'Pina', 'Pinar', 'Pink', 'Piper', 'Portia', 'Princess', 'Priya', 'Prudence',
            'Quinne', 'Queen', 'Qi', 'Quang', 'Quinn', 'Quy',
            'Ruby', 'Rosie', 'Rose', 'Robyn', 'Raelyn', 'Ragna', 'Rianne', 'Rhiana', 'Rachel', 'Raine',
            'Rae', 'Rahel', 'Raisa', 'Ratree', 'Rayen', 'Reanna', 'Reba', 'Reese', 'Regan', 'Regina',
            'Renata', 'Ren', 'Ressia', 'Reva', 'Rena', 'Ria', 'Reni', 'Rica', 'Richelle', 'River',

            'Sally', 'Scarlett', 'Sophia', 'Sophie','Sienna','Sofia','Summer',
            'Sara','Sarah','Selena',
            'Thea', 'Tina', 'Tamara', 'Tori', 'Torey', 'Tabitha', 'Tai', 'Taina', 'Talin', 'Tal', 'Talisha',
            'Talulla', 'Tamela', 'Tamiko', 'Tan', 'Tanaya', 'Tara', 'Tarja', 'Tasia', 'Tatum', 'Teal',
            'Teagan', 'Terez', 'Teresa', 'Theresa', 'Terri',
            'Ulla', 'Ulyssa', 'Ulrika', 'Ubon', 'Uma', 'Unice', 'Undine', 'Unn', 'Urania', 'Usha',
            'Victoria', 'Violet', 'Val', 'Vaida', 'Valda', 'Valierie', 'Vana', 'Vanessa',
            'Willow', 'Wendy', 'Wera', 'Whitley', 'Whitny', 'Watnna',
            'Willa', 'Willow', 'Winona', 'Winter', 'Wook',
            'Ya', 'Yadira', 'Yaffa', 'Yan', 'Yasamin', 'Yancy', 'Yara',
            'Zara', 'Zoe','Zanna', 'Zoe', 'Zan', 'Zaina', 'Zaida', 'Zahara'
        ],

        'm' => [
            'Alfie','Arthur','Archie','Arlo', 'Andrew', 'Albert','Angel',
            'Alexander','Albie','Aaron','Alex','Austin','Albert','Adam',
            'Aiden','Asher','Aldous','Ara','Azarias',
            'Brandon','Braden','Brad','Blake',
            'Balthazar','Barnaby','Bastien','Berlin','Birch','Benjamin',
            'Benjamin','Bobby','Bob','Blake','Brendan','Bastian', 
            'Carson','Charlie','Charles','Charlo','Carter','Caleb','Cody','Chad','Cameron',
            'Corey','Corbin',
            'Darwin','Drexel','Dante','Drake','Dean',
            'Daniel','David','Dylan','Dexter',
            'Donald',
            'Elijah', 'Emerson', 'Ethan', 'Edson','Evan','Enda',
            'Ethan','Elliot','Edward','Ezra','Elijah','Ellis','Elliott',
            'Finch','Fish','Frankie',
            'Felix','Freddie','Finn','Frederick','Finley',
            'George','Gabriel','Garrett','Gary',
            'Harry', 'Heath', 'Henry','Harrison','Harvey','Harley','Hugo','Hunter',
            'Ibrahim','Isaac','Ian',
            'Jack','Jasper','James','Joel','Jose','Jacob','Jean',
            'Jimmy','Jessie','Jeffrey',
            'Justin','John','Jeremy','Jesse','Jake','Jaxon','Jenson','Jordan',
            'Joshua','Joseph','Jack','Jacob','Jayden','Jude','Jesse','Jackson',
            'Kevin','Kevan','Kai',
            'Logan', 'Leo', 'Lee', 'Leonardo','Lucas','Liam', 'Leif',
            'Luca','Luke','Liam','Leon','Lewis','Leonardo','Lucas','Louis','Louie',
            'Mason','Marc','Mark','Mitch','Michael','Morris','Max',
            'Muhammad','Milo','Matteo',
            'Michael','Mohammed','Max','Mason','Nathan','Matthew',
            'Noah','Nome', 'Neil', 'Norman', 'Nick', 'Nathan','Nicholas',
                    
            'Oscar','Ollie','Oliver','Osvaldo','Otto',

            'Patrick','Paul','Peter','Parnel','Peter','Preston','Parker',

            'Quigley','Quin',
            'Rogerio','Riler',
            'Reuben','Reggie','Ronnie','Riley','Rory','Ryan','Roman','Ross',
            'Silas','Scot','Scott','Scotty','Stas','Sam',
            'Sebastian','Samuel','Stanley','Spencer','Simon',
            'Thomas','Theo','Teddy','Theodore','Toby','Tommy','Tyler',
            'William','Will','Walter','Wallace','Wes','Wesley',
            'Zachary','Zeek','Zac','Zack'
        ],
        'surname' => [
            'Astor', 'Albertson', 'Azrea','Acenzune','Aje', 'Alton', 'Alenar','Arns','Alleyne','Arden','Ashton',
            'Birkin','Bond', 'Brown','Blake','Brandt','Brand','Benedict', 'Butler',
            'Bellamy','Bradley','Blent','Barclay','Berkley','Baldock','Basham',
            'Chesterfield', 'Covington', 'Craven', 'Colton','Comey', 'Cook', 'Clark','Cody','Conrad','collin',
            'Darcy', 'Desanti', 'Desoto', 'Donaldson','Dameus', 'Donovan', 'Derby', 'Dorcester',
            'Ellerton', 'Emerts','Edson','Emerson','Easton','Eastwood', 'Erickson','Emison','Ericsson',
            'Finn', 'Fletch', 'Fletcher', 'Foss', 'Fox', 'Forge', 'Filber', 'Franks', 'Frances','Flynn',
            'Fellsmere','Feld','Frost','Freeman','Fleck',
            'Glander', 'Granger', 'Gram', 'Grammer', 'Giuras', 'Garrett','Greene','Ginner',
            'Grebenek','Gent','Gant','Ginter','Gline',
            'Harris', 'Holden', 'Hudson', 'Hogg', 'Hillar', 'Hevard', 'Hoard','Hurston','Hill','Hart','Hessary',
            'Higman','Henderson','Heneghan',
            'Incan', 'Itesco', 'Inker', 'Icardi', 'Irlandis', 'Ikore','Innis','Ilibson',
            'Joseph', 'Juras','Jaski', 'Jockler', 'Jenkins', 'Joons','Jordan',
            'Kilbery','Krassen','Kramer', 'Klosen', 'Kalery','Kroetz',
            'Libardo','Lorenter','Larak','Lemer','Lombardo','Ludo','Lee','Lewis','Landon','Leonardo','Luckett',
            'Lundgren','Locke','Long','Laferty',
            'Marcota', 'Moreso', 'Myers','McMichael','Naranjo','Morata','Merryman','Marshall',
            'Montgomery','Mojo','Miltan','Mason','Matteo','Mitchell','Masters','mills','Moore','Eastman',
            'Neptune', 'Nester', 'Nestor','Nomey', 'Nills','Nirren','Nelson',
            'Olvedersen', 'Oriento','Oberan','Odison', 'Odeon', 'Orr','Oliver','O\'Connel',
            'Pennington', 'Prentas', 'Piedra', 'Pedra', 'Pensker', 'Perreira', 'Poirer','Parker','Prost','Parks',
            'Pearson',
            'Quincy','Quigley','Quail','Quantica',
            'Riflo', 'Ruffington', 'Rigby', 'Rutkowski', 'Ritant','Russo','Readonn',
            'Scott', 'Sembria', 'Spencer', 'Serrano', 'Sims', 'Somme','Sellamy','Sharp',
            'Seed','Sorari','Sabimer','Seyemonaza','Steele','Stevens','Stearman',
            'Tochester', 'Trebright', 'Thurlow','Tesla', 'Teixeira','Thierie','Tanner','Tess','Tor',
            'Ulthbert', 'Underhill', 'Unger', 'Uddenberg', 'Upton','Utillar','Uvellerio',
            'Valyrea', 'Volanteria', 'Volker','Verger', 'Vitori', 'Vans', 'Volland','Vega',
            'Waterhouse', 'Weckler', 'Wester', 'Windsor','White','Wilder','Wright',
            'York','Yibelle','Yellin','Yado', 'Yabo', 'Yorcester',
            'Zewalis', 'Zwelia', 'Zine', 'Zeppler'
        ]
    ];
}
