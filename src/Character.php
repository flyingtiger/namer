<?php
namespace Ryantxr\Storygen\Namer;
use MathPHP\Probability\Distribution\Continuous;

class Character
{

    public $gender;
    public $firstName;
    public $lastName;
    public $age;
    public $educationLevel;
    public $personality;
    public $intelligence;

    function __construct($gender, $options=null)
    {
        if ( $gender == 'any' ) {
            $gender = mt_rand(0,99) > 49 ? 'm' : 'f';
        }
        if ( strtolower($gender) == 'male' ) $gender = 'm';
        if ( strtolower($gender) == 'female' ) $gender = 'f';
        $this->gender = $gender;
        $this->firstName = Generator::generateName($gender);
        $this->lastName = Generator::generateName('surname');
        
        $age = $options['age'] ?? mt_rand(1, 99);
        if ( is_array($age) && count($age) == 2 && is_numeric($age[0]) && is_numeric($age[1]) ) {
            $age = mt_rand($age[0], $age[1]);
        }
        elseif ( $age == 'teen' ) $age = mt_rand(13, 19);
        elseif ( $age == 'child' ) $age = mt_rand(0, 12);
        $this->age = $this->generateAge($age??null);
        $this->educationLevel = $this->generateEducationLevel($this->age);
        $this->intelligence = $this->generateIntelligence();
        $this->personality = new Personality();
        
    }

    function generateAge($ageOption)
    {
        if ( is_numeric($ageOption) ) {
            return $ageOption;
        }
        $lower = 0;
        $upper = 99;
        switch($ageOption) {
            case 'child':
                $upper = 12;
                break;
            case 'teen':
                $lower = 13;
                $upper = 19;
                break;
            case 'middleage':
                $lower = 40;
                $upper = 55;
                break;
            case 'senior':
                $lower = 55;
                $upper = 99;
                break;
            case '20s':
                $lower = 20;
                $upper = 29;
                break;
            case '30s':
                $lower = 30;
                $upper = 39;
                break;
            case '40s':
                $lower = 40;
                $upper = 49;
                break;
            case '50s':
                $lower = 50;
                $upper = 59;
                break;
            case '60s':
                $lower = 60;
                $upper = 69;
                break;
            case '70s':
                $lower = 70;
                $upper = 79;
                break;
            case '80s':
                $lower = 80;
                $upper = 89;
                break;
            case '90s':
                $lower = 90;
                $upper = 99;
                break;
        }
        return mt_rand($lower, $upper);
    }

    function generateEducationLevel($age)
    {
        if ( $age < 5 ) return 0;
        elseif ( $age < 18 ) return $age - 5;
        elseif ( $age < 22 ) {
            if ( mt_rand(0, 99) > 75 ) {
                return $age - 5; // in college
            }
            return 12; // graduated high school
        }
        else {
            if ( mt_rand(0, 99) > 75 ) {
                return 16; // college
            }
            return 12; // graduated high school
        }
    }
    
    function generateIntelligence()
    {
        $σ      = 1;
        $μ      = 0;
        $x      = 2;
        $normal = new Continuous\Normal($μ, $σ);
        $pdf    = $normal->pdf($x);
        $cdf    = $normal->cdf($x);
        $l = mt_rand(0,9);
        $b = mt_rand(0,9);
        return mt_rand(80+$l, 180-$b);
    }

    function name() {
        return $this->firstName . ' ' . $this->lastName;
    }

    function __toString()
    {
        $gender = $this->genderDescription();
        $education = $this->educationDescription();
        return sprintf("Name: %s %s\nAge: %d\nGender: %s\nIntelligence: %d\nEducation: %s\n%s", $this->firstName, $this->lastName, $this->age, $gender, $this->intelligence, $education, $this->personality);
    }

    function data()
    {
        return [
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'age' => $this->age,
            'gender' => $this->genderDescription(),
            'education' => [
                'level' => $this->educationLevel,
                'description' => $this->educationDescription()
            ]
        ];
    }
    function educationDescription()
    {
        if ( $this->educationLevel < 1 ) {
            return 'None';
        }
        if ( $this->educationLevel <= 5 ) {
            return 'Elementary school';
        }
        elseif ( $this->educationLevel <= 8 ) {
            return 'Middle school';
        }
        elseif ( $this->educationLevel <= 12) {

            return 'High school';
        }
        else {
            return 'College';
        }
    }

    function genderDescription()
    {
        $lookup = [
            'm' => 'male',
            'f' => 'female'
        ];
        $gender = $lookup[$this->gender] ?? 'unknown';
        return $gender;
    }
}
