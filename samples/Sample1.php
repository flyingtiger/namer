<?php

require_once __DIR__ . '/../vendor/autoload.php';
use Ryantxr\Storygen\Namer\Character;

$character = new Character('any');
echo $character . "\n";
// print_r($character->data());

$childCharacter = new Character('any', ['age' => 'child']);
echo $childCharacter . "\n";
// print_r($childCharacter->data());

$teenCharacter = new Character('any', ['age' => 'teen']);
echo $teenCharacter . "\n";
// print_r($teenCharacter->data());

$a20Character = new Character('any', ['age' => '20s']);
echo $a20Character . "\n";
// print_r($a20Character->data());

$a30Character = new Character('any', ['age' => '30s']);
echo $a30Character . "\n";
// print_r($a30Character->data());
