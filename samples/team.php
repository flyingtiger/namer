<?php

require_once __DIR__ . '/../vendor/autoload.php';
use Ryantxr\Storygen\Namer\Character;

for ($i=0; $i<11; $i++) {
    $character = new Character('male', ['age' => [17,19]]);
    echo $character->firstName . ' ' . $character->lastName . ' ' .$character->age . "\n";
}