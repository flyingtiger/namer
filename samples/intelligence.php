<?php

// Intelligence

require_once __DIR__ . '/../vendor/autoload.php';

use MathPHP\Probability\Distribution\Continuous;


$σ      = 0.1;
$μ      = 0.4;
$x      = 2;
$normal = new Continuous\Normal($μ, $σ);
for($i=0.0; $i < 1; $i+=0.1) {
    $pdf    = $normal->pdf($i);
    echo "$i   pdf = $pdf\n";
}
